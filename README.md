# Expressque

Yet another typescript router & middleware manager.

## Features

- Full typescript support out of the box
- No external dependencies
- Really small

## Notes

This project in it's _really_ early stages. More features should be available in the next releases.

## Building

Run `npm run build`
