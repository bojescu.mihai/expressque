import { regex } from 'private/config'

export function validator (path: string): boolean {
  const pathIsValid = regex.PATH_REGEX.test(path)
  const lengthIsValid = path.length < 255

  return pathIsValid && lengthIsValid
}
