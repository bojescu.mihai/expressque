export type AndCallback = (callback: () => void) => void;
