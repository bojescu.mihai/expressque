import { ErrorMessage } from 'private/errors'

export class ServerError extends Error {
  status: number;
  body: ErrorMessage;

  constructor (reason: string, status: number, body: ErrorMessage) {
    super(reason)
    this.message = reason
    this.status = status
    this.body = body
  }
}
