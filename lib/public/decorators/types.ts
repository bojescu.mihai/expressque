import { IncomingMessage, Server, ServerResponse } from 'http'
import { Request, Response } from 'public/middleware'
import { Application } from 'public/server'

export interface RequestDecorator {
  decorate: (req: IncomingMessage | Request) => Promise<Request>;
}

export interface ResponseDecorator {
  decorate: (req: ServerResponse | Response) => Promise<Response>;
}

export interface ServerDecorator {
  decorate: (req: Server) => Application;
}
